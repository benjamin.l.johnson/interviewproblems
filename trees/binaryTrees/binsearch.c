#include <sys/types.h>	/* needed for kill */
#include <sys/wait.h>   /* need to properly wait on pid */
#include <signal.h>  	/* proper clean up and killing of child */
#include <stdio.h>		/* for convenience */
#include <stdlib.h>		/* for convenience */
#include <string.h>		/* for convenience */
#include <unistd.h>		/* for close */
#include <errno.h>		/* for error handling */
#include <ctype.h>		/* for tolower and toupper */


typedef struct data_struct
{
	int *array;
	int sizeOfAry;
}data_t;

typedef struct bnode_t
{
	struct bnode_t *left;
	struct bnode_t *right;
	int value;


} bnode_t;

int parseArgs(int ,char **,data_t *);
void treePrint(bnode_t *,bnode_t**);

bnode_t *mkNewNode(int );

bnode_t* printLeftsubTree(bnode_t*);

bnode_t* printRightsubTree(bnode_t*);

void printChildren(bnode_t*);

void iot(bnode_t *);

bnode_t * treeMax(bnode_t*);

bnode_t * treeMin(bnode_t*);

int levelPrint(bnode_t*,int,bnode_t**);


int main(int argc, char** argv)
{

	int *temp;
	data_t data;
	int i=0;

	bnode_t *root,*new_node, **walker;

	parseArgs(argc,argv,&data);

	temp = data.array;

	root = mkNewNode(*temp++);


	for(i=0;i<data.sizeOfAry-1;i++)
	{
		walker = &root;
		new_node = mkNewNode(*temp);

		while(*walker)
		{
			if(new_node->value > (*walker)->value)
			{
				walker = &(*walker)->right;
			}
			else
			{
				walker = &(*walker)->left;
			}
		}
		*walker = new_node;
		//fprintf(stderr, "temp is %d\n",*temp );
		temp++;

	}

	
	/*bnode_t *max = treeMax(root);
	fprintf(stderr, "Max value is %d\n",max->value );
	bnode_t *min = treeMin(root);
	fprintf(stderr, "Min value is %d\n",min->value );
	
	*/
	bnode_t ** vals = calloc(data.sizeOfAry,sizeof(bnode_t *));
	treePrint(root, vals);
	

	//iot(root);


	return 0;
}

bnode_t* mkNewNode(int val)
{
	bnode_t * temp = malloc(sizeof(bnode_t));
	temp->right = NULL;
	temp->left = NULL;
	temp->value = val;
	return temp;
}


void iot(bnode_t *root)
{
	if(!root)
	{
		return;
	}
	iot(root->left);
	fprintf(stderr, "Value is %d\n",root->value);
	iot(root->right);
}


void printChildren(bnode_t *root)
{
	if(root->left)
	{
		printf("left is %d ",root->left->value);
	}

	if(root->right)
	{
		printf(" right is %d ",root->right->value );
	}
	return;
}

int levelPrint(bnode_t *root,int level,bnode_t **data)
{
	int retl=0;
	int retr=0;
	bnode_t** temp;

	if(*(data+level)==NULL)
	{
		//fprintf(stderr, "Level %d: root->value %d\n",level,root->value );
		*(data+level) = calloc((1<<level),sizeof(bnode_t));
	}


	temp = *(data+level);
	while(*temp!=NULL)
	{
		//fprintf(stderr, "root->value is %d\n",(*temp)->value );
		*temp++;
	}
	//fprintf(stderr, "\n" );
	//printf(stderr, "Level %d: root->value %d\n",level,root->value );
	*temp = root;
	

	//fprintf(stderr, "value is %d is level is %d\n",root->value,level );
	if(!root->left && !root->right)
	{
		//fprintf(stderr, "bottom node value is %d level is %d\n",root->value, level );
		return level;
	}

	if(root->left)
	{
		retl = levelPrint(root->left,level+1,data);
	}
	//printf("\n");
	if(root->right)
	{
		retr = levelPrint(root->right,level+1,data);
	}
	return retl > retr ? retl : retr;

}

void treePrint(bnode_t *root, bnode_t **data)
{	
	int level;
	int i=0;
	//int x=0;

	level = levelPrint(root,0,data);
	fprintf(stderr, "level is %d\n",level );
	bnode_t *temp;
	for(i=0;i<level+1;i++)
	{


		temp = data[i];
		//*temp++;
		//fprintf(stderr, "%d\n",(*temp)->value );
		/*for (x = 0; x < (1<<i); x++)
		{	
			//temp = *(data+i);
			
			*temp++;
			
		}*/

		fprintf(stderr, "%d ",(temp->value );	
		fprintf(stderr, "\n" );
		
	}
	//		printChildren(root);
	//int totes_space;
	//totes_space = 1 << size;
	//bnode_t *lef,*rig;

	//fprintf(stderr, "value %d\n",root->value );
	



	//printRightsubTree(root,0);
	/*if(root->left)
	{
		printf("left is %d ",root->left->value);
	}

	if(root->right)
	{
		printf(" right is %d ",root->right->value );
	}
	printf("\n");
	lef = printLeftsubTree(root);
	rig = printRightsubTree(root);

	while(lef || rig)
	{
		if(lef)
		{
			lef = printLeftsubTree(lef);
		}

		if(rig)
		{
			rig = printRightsubTree(rig);
		}
	}*/



	//printLeftsubTree(&root->left,1);
	
	//treePrint(root->right,size);

}

/*
 recursively  get tree max

*/
bnode_t * treeMax(bnode_t* root)
{
	if(!root->right)
	{
		return root;
	}

	return treeMax(root->right);
}


/*
 recursively  get tree min

*/
bnode_t * treeMin(bnode_t* root)
{
	if(!root->left)
	{
		return root;
	}

	return treeMin(root->left);
}


bnode_t *printLeftsubTree(bnode_t* root)
{

	if(!root->left)
	{
		return NULL;
	}
	root = root->left;

	if(root->left)
	{
		printf("left is %d ",root->left->value);
	}

	if(root->right)
	{
		printf(" right is %d ",root->right->value );
	}
	//printf("\n");
	//printLeftsubTree(root->left,1);


	return root;
}

bnode_t* printRightsubTree(bnode_t* root)
{
	
	if(!root->right)
	{
		return NULL;
	}

	root = root->right;

	if(root->left)
	{
		printf("left is %d ",root->left->value);
	}

	if(root->right)
	{
		printf(" right is %d",root->right->value );
	}
	printf("\n");
	//printLeftsubTree(root->left,1);


	return root;
}




int parseArgs(int argc,char **argv,data_t *data)
{
	int i;

	//temporary variable used to walk arrays
	int *walker;
	
	//the first argument is always the filename, so we subtract one
	data->sizeOfAry = argc-1;
	
	//dynamically create array
	data->array = malloc(sizeof(int)*data->sizeOfAry);
	
	//set our temporary variable 
	walker = data->array;
	
	//start at 1 since first arg of argv is always the file name
	for(i=1; i< data->sizeOfAry+1;i++)
	{
		//store the number they passed us into an array element
		*walker++ = atoi(argv[i]);	

	}

	return 0;
}